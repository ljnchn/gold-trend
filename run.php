<?php
require_once __DIR__.'/vendor/autoload.php';

use Hanson\Vbot\Foundation\Vbot;
use Vbot\HotGirl\HotGirl;
use Illuminate\Support\Collection;
use Hanson\Vbot\Message\Text;
use GuzzleHttp\Client;
use Model\GoldRealtime;
use Model\GoldDaily;

$config = require_once __DIR__ . '/config/config.php';
// var_dump($config);die();
$vbot = new Vbot($config);
$vbot->messageExtension->load([
    HotGirl::class,
]);
$messageHandler = $vbot->messageHandler;

// 收到消息触发
$messageHandler->setHandler(function (Collection $message) {

    $client = new Client(['base_uri' => 'http://www.tuling123.com/openapi/api']);
    if ($message['fromType'] == 'Group') {
        if(isset($message['isAt']) && $message['isAt'] && $message['message']){
            if ($message['pure'] == '1') {
                $today = date('Y-m-d') . ' 00:00:00';
                $latest = GoldRealtime::where('time', '>', $today)->orderBy('id', 'desc')->first();
                if (!$latest) {
                    $return = '暂无报价';
                } else {
                    $return = $latest->time . ', price: ' . $latest->price;
                }
            } elseif ($message['pure'] == '呼叫所有人') {
                $return = '呼叫所有人：';
                if (isset($message['from']['MemberList'])) {
                    foreach ($message['from']['MemberList'] as $list) {
                        if ($list['DisplayName']) {
                            $return .= '@' . $list['DisplayName'] . ' ';
                        } else {
                            $return .= '@' . $list['NickName'] . ' ';
                        }
                    }
                }
            } else {
                // 使用图灵机器人回复
                $res = $client->get('http://www.tuling123.com/openapi/api', [
                    'query' => [
                        'key' => 'd21bd0677be44737abb116c1c62a1a09',
                        'info' => $message['pure'],
                        'userid' => mb_substr($message['from']['UserName'], 1, 12),
                    ]
                ]);
                $data = json_decode($res->getBody());
                $return = $data->text;
            }

            Text::send($message['from']['UserName'], $return);
        }
    } elseif ($message['fromType'] == 'Friend' && isset($message['pure']) && $message['pure']) {
        // 使用图灵机器人回复
        $res = $client->get('http://www.tuling123.com/openapi/api', [
            'query' => [
                'key' => 'd21bd0677be44737abb116c1c62a1a09',
                'info' => $message['pure'],
                'userid' => mb_substr($message['from']['UserName'], 1, 12),
            ]
        ]);
        $data = json_decode($res->getBody());
        $return = $data->text;

        Text::send($message['from']['UserName'], $return);
    }
});

// 一直触发
$messageHandler->setCustomHandler(function(){

    if (date('h:i:s') == '08:00:00') {
        $groups = vbot('groups');
        $nickname = '略略略略';
        $group = $groups->getGroupsByNickname($nickname, $blur = false);
        if (isset($group['UserName'])) {
            // 查询金价
            $model = GoldDaily::where('date', '=', date('Y-m-d', time()-3600*24))->first();
            $text = '昨日无行情';
            if ($model) {
                $text = '昨日开盘价：' . $model->open . '，最高价：' . $model->max . '，最低价：' . $model->min . '，收盘价：'. $model->close;
            }
            // 报告金价
            Text::send($group['UserName'], $text);
        }
    }

});

$vbot->server->serve();
