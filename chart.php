<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ChatJS</title>
    <style>
        body{width:960px;height:300px;}
    </style>
</head>
<body>
    <canvas id="myChart"></canvas>
</body>
    <?php
        require_once __DIR__.'/vendor/autoload.php';
        use Model\GoldRealtime;
        $date = date('Y-m-d', time());
        $startTime = strtotime($date . ' 09:30:00');
        $data = [];
        for ($i = 0; $i < 20; $i++) {
            $model = GoldRealtime::where('time', '=', date('Y-m-d H:i:s', $startTime))->first();
            if ($model) {
                $data['labels'][] = date('H:i', $startTime);
                $data['data'][] = $model->price;
            }
            $startTime += 1800;
        }
    ?>
    <script src="asset/js/chart.mini.js"></script>
    <script>
        var data = <?php echo json_encode($data); ?>;
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: [{
                    label: 'price',
                    data: data.data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:false
                        }
                    }]
                }
            }
        });
    </script>
</html>