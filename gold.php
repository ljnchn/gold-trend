<?php
require_once __DIR__.'/vendor/autoload.php';


use GuzzleHttp\Client;
use Model\GoldDaily;
use Model\GoldRealtime;

$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'http://www.sge.com.cn',
    // You can set any number of default request options.
    'timeout'  => 4.0,
]);
 
// $today = date('Y-m-d') . ' 00:00:00';
// $latest = GoldRealtime::where('time', '>', $today)->orderBy('id', 'desc')->first();

// 上海黄金交易所获取数据
$res = $client->post('http://www.sge.com.cn/graph/quotations');
$data = json_decode($res->getBody());
// echo $data->delaystr;die();
$delaystr = str_replace('年', '-', $data->delaystr);
$delaystr = str_replace('月', '-', $delaystr);
$delaystr = str_replace('日', '', $delaystr);
$delaytime = strtotime($delaystr);

foreach ($data->times as $k => $time) {
    if ($data->data[$k]) {
        $time = date('Y-m-d', $delaytime) . ' ' . $time . ':00';
        if (strtotime($time) > time()) {
            continue;
        }
        $model = GoldRealtime::where('time', '=', $time)->first();
        if (!$model) {
            $model = new GoldRealtime();
        }
        $model->time = $time;
        $model->price = $data->data[$k];
        $model->save();
    }
}


if (date('H:i', time()) == '00:00') {
    // 获取历史数据
    $res = $client->post('http://www.sge.com.cn/graph/Dailyhq?instid=Au99.99');
    $data = json_decode($res->getBody());

    foreach ($data->time as $item) {
        $model = GoldDaily::where('date', '=', $item[0])->first();
        if (!$model) {
            $model = new GoldDaily();
        }
        $model->date = $item[0];
        $model->open = $item[1];
        $model->close = $item[2];
        $model->max = $item[3];
        $model->min = $item[4];
        if ($model->save()) {
            // echo $item[0] . "\r\n";
        }
    }
}
