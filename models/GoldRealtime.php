<?php
namespace Model;
use  Illuminate\Database\Eloquent\Model as Eloquent;

require_once('start.php');

class GoldRealtime extends Eloquent
{
    protected $table = 'gold_realtime';
    protected $fillable = ['time', 'price'];
}
