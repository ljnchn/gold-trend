<?php
namespace Model;
use  Illuminate\Database\Eloquent\Model as Eloquent;

require_once('start.php');

class GoldDaily extends Eloquent
{
    protected $table = 'gold_daily';
    protected $fillable = ['date', 'open', 'close', 'max', 'min'];
}
